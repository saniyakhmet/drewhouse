//
//  ViewController.swift
//  drewhouse
//
//  Created by mac on 05.02.2021.
//

import UIKit

class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    let titlesDH = [("corduroy hoodie"), ("secret hoodie"), ("ss corduroy shirt"), ("secret ss tee"), ("corduroy pants"), ("corduroy hat")]
    let desDH = [("100% cotton corduroy"), ("100% cotton"), ("100% cotton corduroy"), ("100% cotton"), ("100% cotton corduroy"), ("100% cotton corduroy")]
    let imagesDH = [UIImage(named: "1"),
                    UIImage(named: "2"),
                    UIImage(named: "3"),
                    UIImage(named: "4"),
                    UIImage(named: "5"),
                    UIImage(named: "6")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titlesDH.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath)
        as! TableViewCell
        cell.imageCell.image = self.imagesDH[indexPath.row]
        cell.titleLabel01.text = self.titlesDH[indexPath.row]
        cell.textLabel02.text = self.desDH[indexPath.row]
        
        return cell
    }
    
    
    
}

